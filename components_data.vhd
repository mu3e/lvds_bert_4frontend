---------------------------------------
--
-- data generator and checker library
-- Jens Kroeger, Dec 2016
--
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------



library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.components_board.all;
use work.components_misc.all;
use work.components_lvds.all;

package components_data is


component data_generator is
	port(
		clk	  : in std_logic_vector(1 downto 0);
		reset_n : in std_logic;
		error_in: in std_logic;
		sig_out : out std_logic_vector(1 downto 0)
		);		
end component;

component data_checker is
	port(
		reset_n 	: in std_logic;
		clk_in	: in std_logic_vector;
		sig_in 	: in std_logic_vector(1 downto 0);
		ch_sel	: in std_logic;
		bert		: out std_logic_vector(127 downto 0);
		clk_out	: out std_logic_vector(1 downto 0);
		rx_locked: out std_logic_vector(1 downto 0);
		rx_dpa_locked : out std_logic_vector(1 downto 0);
		sync_state : out std_logic_vector(1 downto 0)
		);
end component;
	
end package components_data;