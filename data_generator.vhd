-----------------------------------
--
-- DATA GENERATOR:
-- State Machine, counter, encoder, transceiver
--
-- Jens Kroeger, Dec 2016
--
-- 
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.components_board.all;
use work.components_misc.all;
use work.components_lvds.all;
use work.components_8b10b.all;

entity data_generator is
	port (
		-- inputs
		reset_n 	: in std_logic;
		error_in	: in std_logic;
		clk		: in std_logic_vector(1 downto 0);
		
		-- outputs
		sig_out : out std_logic_vector(1 downto 0) -- channel(i) lvds 8b/10b encoded signal
	);
end data_generator;

architecture RTL of data_generator is

	signal sync_counter  	: integer; 								-- use this counter to send k-word n-times
	signal counter_sig		: std_logic_vector(15 downto 0);	-- signal from counter
	signal counter_sig_err 	: std_logic_vector(15 downto 0); -- signal after error injector
	signal sig_enc				: std_logic_vector(19 downto 0);	-- 8b10b encoded
	signal k_en					: std_logic_vector(1 downto 0);	-- k word enable (1 -> send k word, 0 -> do not send k word)
	signal k_en_err			: std_logic_vector(1 downto 0);	-- k wired through error injector
	
	signal start_val 		: unsigned_array8; -- start value for counters
	
	type state_type is (SYNC, READY);
	signal state : state_type;

begin

start_val(0) 	<= "00000000";	-- start counter from 0
start_val(1)	<= "00000000"; -- start counter from 0
--start_val(1)	<= "10000000"; -- start counter from 128 = 256/2

channel_generate: 
for i in 0 to 1 generate

-- counter
cntr_i : counter
	port map(
		clk 		=> clk(i),
		reset_n 	=> reset_n,
		start_val=> start_val(i),
		ctr_sig 	=> counter_sig(i*8+7 downto i*8),
		k_en		=> k_en(i)
	);
	
-- error injection
err_inj_i : error_inject
	port map(
		clk		=> clk(i),
		reset_n	=> reset_n,
		data_in	=> counter_sig(i*8+7 downto i*8),
		error_in => error_in,
		k_in		=> k_en(i),
		data_out	=> counter_sig_err(i*8+7 downto i*8),
		k_out		=> k_en_err(i)
	);
	
	
-- 8b10b encoder
enc_i : encode8b10b
	port map(
		reset_n 	=> reset_n,
		clk 		=> clk(i),
		input 	=> counter_sig_err(i*8+7 downto i*8),
		output 	=> sig_enc(i*10+9 downto i*10),
		k 			=> k_en_err(i)
	);

	
-- LVDS transceiver
lvds_tx_i : lvds_tx
	port map(
		tx_in			=> sig_enc(i*10+9 downto i*10),
		tx_inclock	=> clk(i),
		tx_out	 	=> sig_out(i),
		pll_areset	=> not reset_n
	);
	

end generate;

end RTL;