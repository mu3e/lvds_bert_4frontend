-----------------------------------
--
-- Implementation of BERT:
-- test LVDS links on frontend board
--
--
-- create data stream on STRATIX V board,
-- data checker on FrontEnd board
--
--
-- Jens Kroeger, Dec 2016
-- 
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------
-- This is the TOP LEVEL ENTITY --
----------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.components_board.all;
use work.components_misc.all;
use work.components_lvds.all;
use work.components_data.all;
use work.components_lcd.all;


entity LVDS_BERT_4FrontEnd is
	port (
		-- CLOCKS
		clk_top : in std_logic; -- external clock for top edge		// 125 MHz
		clk_bot : in std_logic; -- external clock for bottom edge	// 125 MHz
		clk_test: in std_logic; -- test clock 
		
		-- DATA
		data_out : out std_logic_vector(1 downto 0); -- one per channel 
		data_in  : in std_logic_vector(1 downto 0); 	-- one per channel 
		
		-- LCD control
		lcd_csn	: 	out 	std_logic;             			--//2.5V    //LCD Chip Select
		lcd_d_cn	: 	out 	std_logic;            			--//2.5V    //LCD Data / Command Select
		lcd_data	: 	inout std_logic_vector(7 downto 0); --//2.5V    //LCD Data
		lcd_wen	:	out 	std_logic;							--//2.5V		//LCD Write Enable
		
		-- LED control
		led_red 		: out std_logic_vector(7 downto 0); -- one per channel // connect to rx_locked
		led_green 	: out std_logic_vector(7 downto 0);  -- one per channel // connect to rx_locked
		
		-- PUSHBUTTONS
		user_pb : in std_logic_vector(1 downto 0); -- active low, PushButtons 5 and 6
		
		-- DIPSWITCH
		dipsw_0 : in std_logic -- dipswitch 0
		
	);
	end LVDS_BERT_4FrontEnd;
	
	
architecture LVDS_BERT_4FrontEnd_arc of LVDS_BERT_4Frontend is
	
	-- Debouncer
	signal user_pb_db	: std_logic_vector(1 downto 0);	-- debounced pushbuttons S5 and S6
	signal dipsw_0_db : std_logic;							-- debounced dipswitch 0
	
	signal reset_n : std_logic;
	signal err_inj : std_logic;
	
	-- LCD stuff
	signal tolcd		: std_logic_vector(127 downto 0);
	signal tolcd_ena	: std_logic;
	signal lcd_busy	: std_logic;
	signal lcd_ready	: std_logic;
	
	-- -- -- 
	signal bert_sig	: std_logic_vector(127 downto 0);
	signal ch_sel		: std_logic;	 						-- channel select
		
	signal clk_in		: std_logic_vector(1 downto 0); -- clock from clk_ctrl
	signal clk_rec		: std_logic_vector(1 downto 0); -- recovered clock from RX
	signal rx_locked	: std_logic_vector(1 downto 0); 
	signal rx_dpa_locked : std_logic_vector(1 downto 0);
	signal sync_state : std_logic_vector(1 downto 0);
	
	
begin

-- clock control
	clk_ctrl_top : component clk_ctrl
		port map (
			inclk  => clk_top,  --  altclkctrl_input.inclk
			outclk => clk_in(0)  -- altclkctrl_output.outclk
		);
	
	clk_ctrl_bot : component clk_ctrl
		port map (
			inclk  => clk_bot,  --  altclkctrl_input.inclk
			outclk => clk_in(1)  -- altclkctrl_output.outclk
		);
	
-- debouncer
db_0: debouncer
	port map(
		clk	=> clk_in(0),
		din	=> user_pb(0),
		dout	=> user_pb_db(0)
	);
	
db_1: debouncer
	port map(
		clk	=> clk_in(0),
		din	=> user_pb(1),
		dout	=> user_pb_db(1)
	);
	

	
db_dip: debouncer
	port map(
		clk	=> clk_in(0),
		din	=> dipsw_0,
		dout	=> dipsw_0_db
	);
	
-- pushbuttons and dipswitch
	reset_n 	<= user_pb_db(0);
	err_inj 	<= not user_pb_db(1);
	
	
-- LCD & LED control
led_green(7) 			<= not reset_n;	-- reset not pressed
led_red(7) 				<= reset_n;			-- reset pressed
led_green(6) 			<= dipsw_0_db;			-- green = channel 0
led_red(6) 				<= not dipsw_0_db;	-- red   = channel 1
	

process (reset_n, clk_in(0))	
begin
	-- channel select:
	if(dipsw_0_db = '1') then	-- dip switch is low-active
		ch_sel <= '0';				-- channel 0
	else
		ch_sel <= '1';				-- channel 1
	end if;
		
	-- reset procedure
	if(reset_n = '0') then
	
		led_green(5 downto 0) <= (others => '1'); -- off
		led_red(5 downto 0)   <= (others => '1'); -- off
	
	elsif(rising_edge(clk_in(0))) then
				
			tolcd <= bert_sig;
			led_green(1 downto 0) <= not rx_locked;		-- on: locked
			led_red(1 downto 0) 	 <= rx_locked;				-- on: not locked
			led_green(3 downto 2) <= not rx_dpa_locked; 	-- on: locked
			led_red(3 downto 2) 	 <= rx_dpa_locked;		-- on: not locked
			led_green(5 downto 4) <= not sync_state;		-- on: READY, off: SYNC
			led_red(5 downto 4)	 <= sync_state;			-- on: SYNC, off: READY
			
	end if;
end process;

tolcd_ena   <= '1';

lcd_ctrl : lcd_controller
	port map(
		din 			=> tolcd,
		clock 		=> clk_in(0),
		reset_n 		=> reset_n,
		enable_in	=> tolcd_ena,
		lcd_enable	=> lcd_csn,
		lcd_rs		=> lcd_d_cn,								
		lcd_rw		=> lcd_wen,					
		busy			=>	lcd_busy,				
		lcd_data		=> lcd_data,
		ready_out	=> lcd_ready
	);
	
-- data generator
data_gen : data_generator
	port map(
		clk		=> clk_in,
		reset_n 	=> reset_n,
		error_in => err_inj,
		sig_out	=> data_out
	);
	
	
-- data checker (BERT & ALIGNMENT)
data_check : data_checker
	port map(
		clk_in 		=> clk_in,
		reset_n 		=> reset_n,
		sig_in 		=> data_in,
		ch_sel		=> ch_sel,
		bert			=> bert_sig,
		clk_out 		=> clk_rec,
		rx_locked	=> rx_locked,
		rx_dpa_locked => rx_dpa_locked,
		sync_state	=> sync_state
	);
	


end LVDS_BERT_4FrontEnd_arc;