--Copyright (C) 2016  Intel Corporation. All rights reserved.
--Your use of Intel Corporation's design tools, logic functions 
--and other software and tools, and its AMPP partner logic 
--functions, and any output files from any of the foregoing 
--(including device programming or simulation files), and any 
--associated documentation or information are expressly subject 
--to the terms and conditions of the Intel Program License 
--Subscription Agreement, the Intel Quartus Prime License Agreement,
--the Intel MegaCore Function License Agreement, or other 
--applicable license agreement, including, without limitation, 
--that your use is for the sole purpose of programming logic 
--devices manufactured by Intel and sold by Intel or its 
--authorized distributors.  Please refer to the applicable 
--agreement for further details.


FUNCTION lvds_rx 
(
	pll_areset,
	rx_cda_reset[0..0],
	rx_channel_data_align[0..0],
	rx_fifo_reset[0..0],
	rx_in[0..0],
	rx_inclock,
	rx_reset[0..0]
)

RETURNS (
	rx_dpa_locked[0..0],
	rx_locked,
	rx_out[9..0],
	rx_outclock
);
