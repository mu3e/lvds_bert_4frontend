lvds_tx_inst : lvds_tx PORT MAP (
		pll_areset	 => pll_areset_sig,
		tx_in	 => tx_in_sig,
		tx_inclock	 => tx_inclock_sig,
		tx_out	 => tx_out_sig
	);
