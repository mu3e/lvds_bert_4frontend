-----------------------------------
--
-- Components for LVDS_BERT_4FrontEnd
-- Jens Kroeger, Dec 2016
-- 
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------

library ieee;
use ieee.std_logic_1164.all;

package components_lvds is

-- EXAMPLE --
--component debouncer is
--	port (
--		clk:	in std_logic;
--		din:	in std_logic;
--		dout: out std_logic
--	);
--end component;




-- LVDS transceiver
	component lvds_tx is
	port (
		tx_in 		: in std_logic_vector(9 downto 0);
		tx_inclock 	: in std_logic;
		pll_areset	: in std_logic;
		tx_out 		: out std_logic
	);
	end component;
	
-- LVDS receiver
	component lvds_rx is
	port (
		pll_areset				 : in std_logic;
		rx_cda_reset			 : in std_logic;
		rx_channel_data_align : in std_logic;
		rx_fifo_reset			 : in std_logic;
		rx_in						 : in std_logic;
		rx_inclock				 : in std_logic;
		rx_reset					 : in std_logic;
		
		rx_dpa_locked			 : out std_logic;
		rx_locked				 : out std_logic;
		rx_out					 : out std_logic_vector(9 downto 0);
		rx_outclock				 : out std_logic
		
	);
	end component;
	


end package components_lvds;