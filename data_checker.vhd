 -----------------------------------
--
-- DATA CHECKER:
-- State Machine, receiver, decoder
--
-- debugging: use loopback card
-- later use: run this on second FPGA
-- 
--Jens Kroeger, Dec 2016
--
-- 
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.components_board.all;
use work.components_misc.all;
use work.components_lvds.all;
use work.components_8b10b.all;
use work.components_lcd.all;

entity data_checker is
	port (
		-- inputs
		reset_n 	: in std_logic;
		clk_in	: in std_logic_vector(1 downto 0);
		sig_in 	: in std_logic_vector(1 downto 0);
		ch_sel	: in std_logic;
		
		-- outputs
		bert		: out std_logic_vector(127 downto 0);		-- bert data going to LCD (received words and error bits)
		clk_out	: out std_logic_vector(1 downto 0); 		-- clock recovered from bitstream in RX
		rx_locked: out std_logic_vector(1 downto 0);			-- 1: rx locked, 0: rx not locked
		rx_dpa_locked : out std_logic_vector(1 downto 0);	-- dynamic phase alignment locked
		sync_state: out std_logic_vector(1 downto 0)			-- sync_state(i) = 0->RESET or SYNC, 1->READY
		
	);
end data_checker;

architecture RTL of data_checker is

	constant k28_5 : std_logic_vector(7 downto 0) := "10111100";

	signal k_en		  	  : std_logic_vector(1 downto 0);	-- k word enable (1 -> send k word, 0 -> do not send k word)
	signal sig_enc   	  : std_logic_vector(19 downto 0);	-- encoded signal from receiver to decoder
	signal sig_dec   	  : std_logic_vector(15 downto 0);	-- decoded signal ready for BERT
	signal sig_exp   	  : logic_array8;							-- expected signal calculated from previously sent word
	signal data_align	  : std_logic_vector(1 downto 0); 	-- 1 -> delay output by 1 bit
	signal clk_rec		  : std_logic_vector(1 downto 0); 	-- clk recovered from data stream
	signal error_vec	  : logic_array8;						 	-- '1' if i-th bit wrong, later count 1s in this vector
	signal n_errors_tmp : unsigned_array8;						-- number of 1s in error vector
	signal start_val	  : logic_array8;							-- initial expectation value of counters = start values
	
	signal n_bytes 	  : unsigned_array64;						-- number of received bytes
	signal n_errors	  : unsigned_array64; 						-- number of wrong bits
	
	signal tofifo 		  : logic_array128; 						-- data to fifo
	signal tomux		  : logic_array128;						-- data to mux
	
	signal rx_reset	  : std_logic_vector(1 downto 0);
	signal rx_locked_sig: std_logic_vector(1 downto 0);
	
	signal align_ctr : unsigned_array64;
	signal k_seen	  : unsigned_array8;
	signal k_notseen : unsigned_array8;
	
	type state_type is (RESET, SYNC, READY);
	type state_array is array (1 downto 0) of state_type;
	signal state : state_array;
	
	
	-- count number of ones in error vector
	function count_ones(s : std_logic_vector) return integer is
		variable temp : integer := 0;
	begin
		for i in s'range loop
			if s(i) = '1' then 
				temp := temp + 1; 
			end if;
		end loop;
		return temp;
	end function count_ones;
	
begin

	start_val(0) 	<= "00000000";  -- start from 0
	start_val(1) 	<= "10000000";  -- start from 128
	

channel_generate: 
for i in 0 to 1 generate

rx_reset(i) <= (not reset_n) and (not rx_locked_sig(i));
clk_out(i) <= clk_rec(i);

-- LVDS receiver
lvds_rx_i : lvds_rx
	port map(
		pll_areset 					=> not reset_n,
		rx_cda_reset 				=> not reset_n,
		rx_channel_data_align 	=> data_align(i),
		rx_fifo_reset				=> not reset_n,
		rx_in 						=> sig_in(i),
		rx_inclock 					=> clk_in(i),
		rx_reset						=> rx_reset(i),
		
		rx_dpa_locked				=> rx_dpa_locked(i),
		rx_locked 					=> rx_locked_sig(i),
		rx_out 						=> sig_enc(i*10+9 downto i*10),
		rx_outclock 				=> clk_rec(i)
		
	);

rx_locked(i) <= rx_locked_sig(i);
	
-- 8b10b decoders
dec_i : decode8b10b
	port map(
		reset_n 	=> reset_n,
		clk 		=> clk_rec(i),
		input 	=> sig_enc(i*10+9 downto i*10),
		output 	=> sig_dec(i*8+7 downto i*8),
		k 			=> k_en(i)
	);
	
---------------------------------------
-- state machine -- ALIGNMENT & BERT --
---------------------------------------
process(k_en(i), clk_rec(i), reset_n, state(i))
begin
	if((reset_n = '0') or (state(i) = RESET)) then
		
		state(i) 		<= SYNC;
		sync_state(i)	<= '0';
		n_bytes(i) 		<= (others => '0');
		n_errors(i) 	<= (others => '0');
		n_errors_tmp(i)<= (others => '0');
		error_vec(i)	<= (others => '0');
		data_align(i) 	<= '0';
		align_ctr(i)	<= (others => '0');
		k_seen(i)		<= (others => '0');
		k_notseen(i)	<= (others => '0');
		
		sig_exp(i) <= start_val(i);
		
	elsif(rising_edge(clk_rec(i))) then
	
		if(state(i) = SYNC) then -- synchronisation
			sync_state(i) <= '0';
		
			if((sig_dec(i*8+7 downto i*8) = k28_5) and (k_en(i) = '1')) then
			
				data_align(i) <= '0';
			
				if(k_seen(i) = 0) then
				
					align_ctr(i) <= (others => '0');
					k_seen(i) <= k_seen(i) + 1;
				
				elsif(align_ctr(i) = 256) then
				
					align_ctr(i) <= (others => '0');
					k_seen(i) <= k_seen(i) + 1;
					
					if(k_seen(i) = 10) then		-- find 10 k-words "in a row"
					
						state(i) <= READY;
						sync_state(i) <= '1';
						k_seen(i) <= (others => '0');
						k_notseen(i) <= (others => '0');
						align_ctr(i) <= (others => '0');
						
					end if;
					
				end if;
		
			else -- not = k-word
			
				if(align_ctr(i) = 256) then
				
					align_ctr(i) 	<= (others => '0');
					k_seen(i)		<= (others => '0');
					data_align(i) 	<= '1';
				
				else
				
					data_align(i) 	<= '0';
					align_ctr(i) <= align_ctr(i) + 1;
					
				end if;
			
			end if; -- SYNC
			
		else -- state = READY -> actual BERT
		
			data_align(i) 	 <= '0';															-- no bit shifts
		
			if(k_en(i) = '0') then -- data coming in
				error_vec(i) 	 <= sig_exp(i) xor sig_dec(i*8+7 downto i*8);	-- build error vector
				n_errors_tmp(i) <= to_unsigned(count_ones(error_vec(i)),8);		-- count ones in error vector
				sig_exp(i)		 <= sig_exp(i) + 1;										-- increment by 1
				align_ctr(i)	 <= align_ctr(i) + 1;									-- to check sync
				
				if(align_ctr(i) = 256) then
					k_notseen(i) <= k_notseen(i) + 1;									-- there should be a k-word after 256 words
					align_ctr(i) <= (others => '0');										-- reset counter
				end if;
				
			else -- if(k_en = '1') -- k-word coming in
				error_vec(i)	<= k28_5 xor sig_dec(i*8+7 downto i*8);			-- build error vector
				n_errors_tmp(i)<= to_unsigned(count_ones(error_vec(i)),8);		-- count ones in error vector
				sig_exp(i)		<= start_val(i);											-- set expected signal to start value
				
				-- check for sync
				if((n_errors_tmp(i) = 0) and (align_ctr(i) = 256)) then			-- i.e. sig = k-word
					k_notseen(i) <= (others => '0');										-- reset counter
					align_ctr(i) <= (others => '0');										-- reset counter
				
				else -- error bits															-- k-word flawed
					k_notseen(i) <= k_notseen(i) + 1;
				
				end if;
				
			end if;
			
			if(k_notseen(i) = 5) then														-- no or flawed k-word for 5 times in a row
				state(i) <= SYNC;
				k_notseen(i) <= (others => '0');
				align_ctr(i) <= (others => '0');
			end if;
			
			n_bytes(i) <= n_bytes(i) + 1;
			n_errors(i) <= n_errors(i) + n_errors_tmp(i);
			  
		end if; -- state = READY
					
		tofifo(i) <= std_logic_vector(n_errors(i)) & std_logic_vector(n_bytes(i));
		
	end if;
end process;


fifo_i : fifo
	port map(
		data 		=> tofifo(i),
		wrreq 	=> clk_rec(i),
		wrclk		=> clk_rec(i),
		rdreq 	=> clk_in(0),
		rdclk 	=> clk_in(0),		-- clk which is used for lcd control
		q 			=> tomux(i)
	);

end generate;

my_lcd_mux : lcd_mux
	port map(
		data0x => tomux(0),
		data1x => tomux(1),
		sel 	 => ch_sel,
		result => bert
	);


end RTL; 