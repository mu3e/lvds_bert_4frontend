----------------------------------------------------------------------------
-- 4 bit to hex coneverter for the LCD display
--
-- Niklaus Berger, Heidelberg University
-- nberger@physi.uni-heidelberg.de
--
-- Based on version by Jens Petersen
--
-----------------------------------------------------------------------------

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.components_lcd.all;

entity lcd_hex_converter is
	port(
		din:			in std_logic_vector(3 downto 0);
		dout:			out std_logic_vector(7 downto 0)
		);		
end entity lcd_hex_converter;



architecture rtl of lcd_hex_converter is	


begin


with din select
dout <= 	num0 when "0000",
			num1 when "0001",
			num2 when "0010",
			num3 when "0011",
			num4 when "0100",
			num5 when "0101",
			num6 when "0110",
			num7 when "0111",
			num8 when "1000",
			num9 when "1001",		
			acap when "1010",
			bcap when "1011",
			ccap when "1100",
			dcap when "1101",
			ecap when "1110",
			fcap when "1111";

end rtl;