-----------------------------------------------------------------------------
-- Controller for the LCD display
--
-- Niklaus Berger, Heidelberg University
-- nberger@physi.uni-heidelberg.de
--
-- Based on version by Jens Petersen
--
-----------------------------------------------------------------------------

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.components_lcd.all;

entity lcd_controller is
	port(
		
		
		din:			in std_logic_vector(127 downto 0);
		
		clock: in std_logic;												-- Reference Clock
		reset_n: in std_logic;											-- Manual reset_n
		enable_in: in std_logic;										-- external LCD enable
		lcd_enable: out std_logic;										-- On/Off Switch, connect to lcd_csn, 1 is on, 0 is off
		lcd_rs: out std_logic;											-- Data/Setup Switch, connect to lcd_d_cn, 1 is data, 0 is setup
		lcd_rw: out std_logic;											-- Read/Write Switch, connect to lcd_wen, 1 is read, 0 is write
		busy: out std_logic;												-- Busy Feedback
		lcd_data: out std_logic_vector (7 downto 0);					-- LCD Data
		ready_out: out std_logic										-- ready signal
		
		);		
end entity lcd_controller;



architecture rtl of lcd_controller is	
	
	type control_state is (
		power_up,
		init,
		selector,
		switch_lines,
		ready,
		send,
		idle);
	signal 		state: control_state := power_up;
	constant 	freq:	integer := 125;									-- set to clock MHz value
	
	subtype char_vector is std_logic_vector(7 downto 0);
	type char_array is array(0 to 31) of char_vector;
	signal char: char_array;	
	
	signal clock_count: integer := 0;
	signal send_count: integer := 0;
	signal second_line: std_logic := '0';
	signal one_line: std_logic := '0';

begin

	converts:
	for i in 31 downto 0 generate
	
	conv:lcd_hex_converter
	port map(
		din	=>	din(i*4+3 downto i*4),
		dout 	=> char(31-i)
		);		
	
	end generate;


	
	control_sequence: process(clock, reset_n, enable_in) is
				
		
	begin
	if (reset_n = '0') then
	
		state <= power_up;
		clock_count <= 0;
		
	elsif rising_edge(clock) then
					
		case state is
					
			when power_up =>											-- 50ms wait to ensure supply voltage has risen
				ready_out <= '0';
				busy <= '1';
				if (clock_count < (50000 * freq)) then
					clock_count <= clock_count + 1;
					state <= power_up;
				else
					clock_count <= 0;
					send_count <= 0;
					lcd_rs <= '1';
					lcd_rw <= '0';
					lcd_data <= "00000000";
					state <= init;
				end if;	
							
				
			when init =>
				busy <= '1';
				clock_count <= clock_count + 1;
				one_line	<= '0';
				lcd_rs <= '0';
				if (clock_count < (10 * freq)) then						-- set function
					lcd_data <= "00111100";								-- 2-line mode, display on
					lcd_enable <= '1';
					state <= init;
				elsif (clock_count < (60 * freq)) then					-- 50us wait
					lcd_data <= "00111100";
					lcd_enable <= '0';
					state <= init;
				elsif (clock_count < (70 * freq)) then					-- display, cursor, blink
					lcd_data <= "00001100";      						--display on, cursor off, blink off
					lcd_enable <= '1';
					state <= init;
				elsif (clock_count < (120 * freq)) then					-- 50us wait
					lcd_data <= "00001100";
					lcd_enable <= '0';
					state <= init;
				elsif (clock_count < (130 * freq)) then					-- clear display
					lcd_data <= "00000001";
					lcd_enable <= '1';
					state <= init;
				elsif (clock_count < (2130 * freq)) then				-- 2ms wait
					lcd_data <= "00000001";
					lcd_enable <= '0';
					state <= init;
				elsif (clock_count < (2140 * freq)) then				-- set entry mode
					lcd_data <= "00000110";      						--increment mode, entire shift off
					lcd_enable <= '1';
					state <= init;
				elsif (clock_count < (2200 * freq)) then				-- 60us wait
					lcd_data <= "00000110";
					lcd_enable <= '0';
					state <= init;
				elsif (clock_count < (2400 * freq)) then				-- set DDRAM address
					lcd_data <= "10000000";
					lcd_enable <= '1';
				elsif (clock_count < (2460 * freq)) then				-- 60us wait
					lcd_data <= "10000000";
					lcd_enable <= '0';
					state <= init;
				else													-- initialization complete
					clock_count <= 0;
					busy <= '0';
					state <= selector;
				end if;
				
				
			when selector =>
				if (send_count = 16 and second_line = '0' and one_line = '0') then			-- switch to second line
					state <= switch_lines;
				elsif (send_count > 31 or (send_count > 15 and one_line = '1')) then
					send_count <= 0;
					--second_line := '0';
					state <= switch_lines;										-- for updatable display data
				else
					state <= ready;
				end if;
				
				
			when switch_lines =>										-- second line initialization
				busy <= '1';
				if (clock_count < (50000 * freq)) then					-- 50ms wait
					clock_count <= clock_count + 1;
					state <= switch_lines;
				elsif (clock_count < (50050 * freq)) then				-- set DDRAM address
					lcd_rs <= '0';
					lcd_rw <= '0';
					if(second_line = '0') then
						lcd_data <= "11000000";
					else
						lcd_data <= "10000000";
					end if;
					lcd_enable <= '1';
					clock_count <= clock_count + 1;
					state <= switch_lines;
				elsif (clock_count < (50100 * freq)) then				-- 50us wait
					lcd_enable <= '0';
					lcd_data <= "00000000";
					clock_count <= clock_count + 1;
					state <= switch_lines;
				else
					clock_count <= 0;
					second_line <= not second_line;
					state <= selector;
				end if;
				
				
			when ready =>
				if (enable_in = '1') then
					busy <= '1';
					lcd_rs <= '1';
					lcd_rw <= '0';
					lcd_data <= char(send_count);
					clock_count <= 0;
					state <= send;
				else
					busy <= '0';
					lcd_rs <= '0';
					lcd_rw <= '0';
					clock_count <= 0;
					lcd_data <= "00000000";
					state <= selector;
				end if;
						
				
			when send =>												-- send data
				busy <= '1';
				if (clock_count < (50 * freq)) then
					busy <= '1';
					if (clock_count < freq) then
						lcd_enable <= '0';
					elsif (clock_count < (14 * freq)) then
						lcd_enable <= '1';
					elsif (clock_count < (27 * freq)) then
						lcd_enable <= '0';
					end if;
					clock_count <= clock_count + 1;
					state <= send;
				else
					send_count <= send_count + 1;
					clock_count <= 0;
					ready_out <= '1';
					state <= selector;
				end if;
				
				
			when idle =>
				if (reset_n = '0') then
					ready_out <= '0';
					state <= init;
					clock_count <= 0;
				elsif (send_count > 31 or (send_count > 15 and one_line = '1')) then
					ready_out <= '1';
					busy <= '0';
					state <= idle;
					clock_count <= clock_count + 1;
				end if;
						
				
		end case;
		
	end if;
	end process control_sequence;
	
end architecture rtl;
				
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					