---------------------------------------------------------
-- 				Error injection entity    			 		 --
--    						               					 --
--                                                     --
-- Sebastian Dittmeier, Heidelberg University 2015     --
-- dittmeier@physi.uni-heidelberg.de                   --
--																		 --
-- modified by Jens Kroeger, Jan 2017						 --
---------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;


entity error_inject is 
	port (
		clk						: in std_logic;
		reset_n					: in std_logic;
		data_in					: in std_logic_vector(7 downto 0);
		error_in					: in std_logic;
		k_in						: in std_logic;
		data_out					: out std_logic_vector(7 downto 0);
		k_out						: out std_logic
	);	
end error_inject;

ARCHITECTURE RTL of error_inject is

signal 	error_edge : std_logic_vector(1 downto 0);
	
begin
	error_process : process(clk, reset_n)
	begin
		if(reset_n = '0')then
			data_out 	<= (others => '0');
			k_out			<= '0';
			error_edge 	<= (others =>'0');
		elsif(rising_edge(clk))then
			data_out 	<= data_in;
			k_out			<= k_in;
			error_edge	<= error_edge(0) & error_in;
			if(error_edge = "01" and k_in='0')then
				data_out <= data_in(7 downto 1) & not data_in(0);
			end if;
		end if;
	end process error_process;

end RTL;
