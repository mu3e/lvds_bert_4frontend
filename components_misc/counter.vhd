-----------------------------------
--
-- 8-bit counter
-- Jens Kroeger, Dec 2016
-- 
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
	port (
		clk		: in std_logic;
		reset_n	: in std_logic;
		start_val: in unsigned(7 downto 0);	-- value from which counter starts
		ctr_sig 	: out std_logic_vector(7 downto 0);	-- counter signal: 0-255 or k28.5
		k_en		: out std_logic							-- 1: send k-word, 0: send counter
	);
end counter;

architecture counter_arc of counter is

	constant k28_5 : unsigned(7 downto 0) := "10111100";
	signal ctr : unsigned(7 downto 0);
	signal k	  : std_logic;
	
begin	

process(clk)
	begin
		-- routine: send k-word, then send 0 to 255, send k-word, ...
		
		if(reset_n = '0') then
			ctr <= k28_5;
			k <= '1';

		elsif(rising_edge(clk)) then
		
			if (k = '0' and ctr = start_val+255) then
				ctr <= k28_5;
				k <= '1';
				
			elsif(k = '1') then
				ctr <= start_val; 
				k <= '0';
				
			else
				ctr <= ctr + 1;
				k <= '0';
				
			end if;
			
			ctr_sig <= std_logic_vector(ctr);
			k_en <= k;
			
		end if;
		
	end process;
	
end counter_arc;