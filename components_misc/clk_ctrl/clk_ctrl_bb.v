
module clk_ctrl (
	inclk,
	outclk);	

	input		inclk;
	output		outclk;
endmodule
