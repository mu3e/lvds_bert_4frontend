---------------------------------------
--
-- miscellaneous board component library
-- Jens Kroeger, Dec 2016
--
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package components_misc is

type int_array is array (1 downto 0) of integer;
type unsigned_array64 is array(1 downto 0) of unsigned(63 downto 0);
type unsigned_array8 is array(1 downto 0) of unsigned(7 downto 0);
type logic_array8 is array(1 downto 0) of std_logic_vector(7 downto 0);
type logic_array64 is array (1 downto 0) of std_logic_vector(63 downto 0);
type logic_array128 is array (1 downto 0) of std_logic_vector(127 downto 0);

-- counter
	component counter is
	port (
		clk 		: in std_logic;
		reset_n 	: in std_logic;
		start_val: in unsigned(7 downto 0);
		ctr_sig 	: out std_logic_vector(7 downto 0);
		k_en		: out std_logic
	);
	end component;
	
-- error injector
	component error_inject is
	port (
		clk		: in std_logic;
		reset_n 	: in std_logic;
		data_in 	: in std_logic_vector(7 downto 0);
		error_in : in std_logic;
		k_in		: in std_logic;
		data_out	: out std_logic_vector(7 downto 0);
		k_out		: out std_logic
	);
	end component;
	
-- clock control
	component clk_ctrl is
		port (
			inclk  : in  std_logic := 'X'; -- inclk
			outclk : out std_logic         -- outclk
		);
	end component clk_ctrl;
	
end package components_misc;