
# Clock constraints
create_clock -name "clk_top" 	-period 8.000ns [get_ports {clk_top}]
create_clock -name "clk_bot" 	-period 8.000ns [get_ports {clk_bot}]
create_clock -name "clk_test" -period 8.000ns [get_ports {clk_test}]

# Automatically constrain PLL and other generated clocks
derive_pll_clocks -create_base_clocks

# Automatically calculate clock uncertainty to jitter and other effects.
derive_clock_uncertainty

set_false_path -to {led_green[*]~reg0}
set_false_path -to {led_red[*]~reg0}