---------------------------------------
--
-- On detector FPGA for layer 0 - board component library
-- Jens Kroeger, Dec 2016
--
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------



library ieee;
use ieee.std_logic_1164.all;

package components_board is


component debouncer is
	port(
		clk:			in std_logic;
		din:			in std_logic;
		dout:			out std_logic
		);		
end component;

	
end package components_board;