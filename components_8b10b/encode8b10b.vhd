-----------------------------------
--
-- 8b10b Encoder
-- Wrapper for open core 8bit/10bit
-- Jens Kroeger, Dec 2016
-- 
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.components_8b10b.all;

entity encode8b10b is
	port (
		reset_n		: in std_logic;
		clk			: in std_logic;
		input			: in std_logic_vector(7 downto 0);
		output		: out std_logic_vector(9 downto 0);
		k				: in std_logic
	);
end encode8b10b;

architecture RTL of encode8b10b is

begin

oc8b10b_enc : enc_8b10b
	port map(
		RESET 	=> not reset_n,
		SBYTECLK => clk,
		AI 		=> input(0),
		BI 		=> input(1),
		CI 		=> input(2),
		DI 		=> input(3),
		EI 		=> input(4),
		FI 		=> input(5),
		GI 		=> input(6),
		HI 		=> input(7),
		KI			=> k,
		AO 		=> output(0),
		BO 		=> output(1),
		CO 		=> output(2),
		DO 		=> output(3),
		EO			=> output(4),
		IO			=> output(5),
		FO 		=> output(6),
		GO 		=> output(7),
		HO 		=> output(8),
		JO 		=> output(9)
	);
		
end RTL;