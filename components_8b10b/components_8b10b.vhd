---------------------------------------
--
-- 8bit/10bit decoding/encoding component library
-- Jens Kroeger, Dec 2016
--
-- kroeger@physi.uni-heidelberg.de
--
----------------------------------



library ieee;
use ieee.std_logic_1164.all;

package components_8b10b is

-- 8b/10b encoder
	component enc_8b10b is
	port (
		RESET 											: in std_logic ;		-- Global asynchronous reset (active high) 
		SBYTECLK 										: in std_logic ;	-- Master synchronous send byte clock
		KI 												: in std_logic ;			-- Control (K) input(active high)
		AI, BI, CI, DI, EI, FI, GI, HI 			: in std_logic ;	-- Unencoded input data
		JO, HO, GO, FO, IO, EO, DO, CO, BO, AO : out std_logic 	-- Encoded out 
	);
	end component;

	component encode8b10b is
	port (
		reset_n 	: in std_logic;
		clk		: in std_logic;
		input		: in std_logic_vector(7 downto 0);
		output 	: out std_logic_vector(9 downto 0);
		k 			: in std_logic
		);
	end component;

-- 8b/10b decoder
	component dec_8b10b is
    port(
		RESET 									: in std_logic ;	-- Global asynchronous reset (AH) 
		RBYTECLK 								: in std_logic ;	-- Master synchronous receive byte clock
		AI, BI, CI, DI, EI, II 				: in std_logic ;
		FI, GI, HI, JI 						: in std_logic ; -- Encoded input (LS..MS)		
		KO 										: out std_logic ;	-- Control (K) character indicator (AH)
		HO, GO, FO, EO, DO, CO, BO, AO 	: out std_logic 	-- Decoded out (MS..LS)
	    );
	end component;

	component decode8b10b is
	port (
		reset_n				: in std_logic;
		clk					: in std_logic;
		input					: in std_logic_vector (9 downto 0);
		output				: out std_logic_vector (7 downto 0);
		k						: out std_logic
	);
	end component;
	
	
end package components_8b10b;